@echo off
cls

rgbasm -o build/main.o src/main.asm
rgblink -p00 -o build/game.gb build/main.o
rgbfix -p00 -v -t game build/game.gb

del %~dp0\build\main.o /q