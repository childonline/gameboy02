include "include/gb/constants.inc"

section "vblank_interrupt", ROM0[$0040]
    ret

section "lcd_interrupt", ROM0[$0048]
    ret

section "timer_interrupt", ROM0[$0050]
    ret

section "serial_interrupt", ROM0[$0058]
    ret

section "joypad_interrupt", ROM0[$0060]
    ret

section "rom_boot", ROM0[$0100]
    jp main

section "rom_header", ROM0[$0104]
    DB $CE, $ED, $66, $66, $DC, $CC, $6E, $E6   ; $0104 - $0133: The Nintendo Logo.
    DB $CC, $0D, $00, $0B, $DD, $DD, $D9, $99
    DB $03, $73, $00, $83, $BB, $BB, $67, $63
    DB $00, $0C, $00, $0D, $6E, $0E, $EC, $CC
    DB $00, $08, $11, $1F, $DD, $DC, $99, $9F
    DB $88, $89, $00, $0E, $BB, $B9, $33, $3E
    DB "0123456789ABCDE"                        ;$0134 - $0142: The cartrtidge title.
    DB $00                                      ;$0143:         Gameboy Color compatibility flag.
    DB "00"                                     ;$0144 - $0145: Licensee Code, a two character name.
    DB $00                                      ;$0146:         Super Gameboy compatibility flag.
    DB $00                                      ;$0147:         Cartridge type. Either no ROM or MBC5 is recommended.
    DB $00                                      ;$0148:         Rom size.
    DB $00                                      ;$0149:         Ram size.
    DB $01                                      ;$014A:         Destination code.
    DB $33                                      ;$014B:         Old licensee code.
    DB $00                                      ;$014C:         ROM version number
    DB $00                                      ;$014D:         Header checksum.
    DW $00                                      ;$014E- $014F:  Global checksum.

include "include/gb/debug.inc"

include "src/core/memory.asm"
include "src/core/lcd.asm"
include "src/core/joypad.asm"

include "src/game/menu.asm"
include "src/game/game.asm"

main::
    jp menu

menu::
    call wait_vblank
    call lcd_off
    call clear_oam
    call clear_bg_map
    call lcd_ini
    call load_menu_data
    call lcd_on
.menu_loop:
    call wait_vblank
    call update_menu
    jr .menu_loop

game::
    call wait_vblank
    call lcd_off
    call clear_bg_map
    call load_game_data
    call lcd_on
.game_loop:
    call wait_vblank
    call update_game
    jr .game_loop
