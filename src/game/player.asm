section "player", rom0
include "include/assets/player.inc"

;constants
OAM_OFFSET equ $00
PLAYER_START_X equ (160 / 2 - 20)
PLAYER_START_Y equ (144 / 2 + 16)

; variables
player_x:: DS 1
player_y:: DS 1

init_player::

    ; load player sprite to VRAM (mixed)
    ld de, $11              ; size in bytes (should be 16 bytes, but displays ok only if 17 bytes wtf?)
    ld bc, player_tile_data ; source address
    ld hl, $8800            ; destination address
    call memcpy

    ; load player sprite to OAM at offset 0
    ld  hl, _OAMRAM + OAM_OFFSET
    ld  [hl], 72
    ld  hl, _OAMRAM + OAM_OFFSET + 1
    ld  [hl], 40
    ld  hl, _OAMRAM + OAM_OFFSET + 2
    ld  [hl], $80
    ld  hl, _OAMRAM + OAM_OFFSET + 3
    ld  [hl], $00
    ret

update_player::
    ld  hl, _OAMRAM + 1
    inc [hl]; set X += 1
    ret