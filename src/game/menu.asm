include "include/assets/menu.inc"

update_menu::
    call read_joypad
    and	PADF_START
    jp nz, game
    ret

load_menu_data::
    ; load menu map to VRAM (background)
    ld de, $0800
    ld bc, menu_tile_data
    ld hl, $9000 ; VRAM_TILES_BG
    call memcpy

    ; load bottom tile map to VRAM (mixed)
    ld de, (menu_tile_data_size - $0800)
    ld bc, (menu_tile_data + $0800) 
    ld hl, $8800 ; VRAM_TILES_MIX
    call memcpy

    ; load menu tile data
    call .load_menu_tiles
    ret

; load menu tiles line by line
; TODO: refactor into some sort of loop
.load_menu_tiles:
    ld de, $15
    ld bc, menu_map_data + ($14 * $0)
    ld hl, $9C00
    call memcpy
    ld de, $15
    ld bc, menu_map_data + ($14 * $1)
    ld hl, $9C20
    call memcpy
    ld de, $15
    ld bc, menu_map_data + ($14 * $2)
    ld hl, $9C40
    call memcpy
    ld de, $15
    ld bc, menu_map_data + ($14 * $3)
    ld hl, $9C60
    call memcpy
    ld de, $15
    ld bc, menu_map_data + ($14 * $4)
    ld hl, $9C80
    call memcpy
    ld de, $15
    ld bc, menu_map_data + ($14 * $5)
    ld hl, $9CA0
    call memcpy
    ld de, $15
    ld bc, menu_map_data + ($14 * $6)
    ld hl, $9CC0
    call memcpy
    ld de, $15
    ld bc, menu_map_data + ($14 * $7)
    ld hl, $9CE0
    call memcpy
    ld de, $15
    ld bc, menu_map_data + ($14 * $8)
    ld hl, $9D00
    call memcpy
    ld de, $15
    ld bc, menu_map_data + ($14 * $9)
    ld hl, $9D20
    call memcpy
    ld de, $15
    ld bc, menu_map_data + ($14 * $A)
    ld hl, $9D40
    call memcpy
    ld de, $15
    ld bc, menu_map_data + ($14 * $B)
    ld hl, $9D60
    call memcpy
    ld de, $15
    ld bc, menu_map_data + ($14 * $C)
    ld hl, $9D80
    call memcpy
    ld de, $15
    ld bc, menu_map_data + ($14 * $D)
    ld hl, $9DA0
    call memcpy
    ld de, $15
    ld bc, menu_map_data + ($14 * $E)
    ld hl, $9DC0
    call memcpy
    ld de, $15
    ld bc, menu_map_data + ($14 * $F)
    ld hl, $9dE0
    call memcpy
    ld de, $15
    ld bc, menu_map_data + ($14 * $10)
    ld hl, $9E00
    call memcpy
    ld de, $15
    ld bc, menu_map_data + ($14 * $11)
    ld hl, $9E20
    call memcpy
    ret