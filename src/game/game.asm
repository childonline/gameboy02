include "include/assets/tileset.inc"
include "include/assets/levels.inc"
include "src/game/player.asm"

update_game::
    call read_joypad
    call update_player
    ret

    ;call update_player
    ; jr .update_game_loop

load_game_data::
    ; disable window & bg display and enable only bg display
    ld hl, rLCDC 
    set 3, [hl] ; bg display - on
    res 4, [hl] ; bg & window display - off

    ; set palette
    ld hl, rBGP
    ld [hl], %11100100

    ; load level map to vram (background)
    ld de, $0800
    ld bc, tileset_data
    ld hl, $9000 ; VRAM_TILES_BG
    call memcpy

    ; load menu tile data
    call .load_level_tiles

    call init_player
    ret

.load_level_tiles:
    ld de, $15
    ld bc, island_map_data + ($14 * $0)
    ld hl, $9C00
    call memcpy
    ld de, $15
    ld bc, island_map_data + ($14 * $1)
    ld hl, $9C20
    call memcpy
    ld de, $15
    ld bc, island_map_data + ($14 * $2)
    ld hl, $9C40
    call memcpy
    ld de, $15
    ld bc, island_map_data + ($14 * $3)
    ld hl, $9C60
    call memcpy
    ld de, $15
    ld bc, island_map_data + ($14 * $4)
    ld hl, $9C80
    call memcpy
    ld de, $15
    ld bc, island_map_data + ($14 * $5)
    ld hl, $9CA0
    call memcpy
    ld de, $15
    ld bc, island_map_data + ($14 * $6)
    ld hl, $9CC0
    call memcpy
    ld de, $15
    ld bc, island_map_data + ($14 * $7)
    ld hl, $9CE0
    call memcpy
    ld de, $15
    ld bc, island_map_data + ($14 * $8)
    ld hl, $9D00
    call memcpy
    ld de, $15
    ld bc, island_map_data + ($14 * $9)
    ld hl, $9D20
    call memcpy
    ld de, $15
    ld bc, island_map_data + ($14 * $A)
    ld hl, $9D40
    call memcpy
    ld de, $15
    ld bc, island_map_data + ($14 * $B)
    ld hl, $9D60
    call memcpy
    ld de, $15
    ld bc, island_map_data + ($14 * $C)
    ld hl, $9D80
    call memcpy
    ld de, $15
    ld bc, island_map_data + ($14 * $D)
    ld hl, $9DA0
    call memcpy
    ld de, $15
    ld bc, island_map_data + ($14 * $E)
    ld hl, $9DC0
    call memcpy
    ld de, $15
    ld bc, island_map_data + ($14 * $F)
    ld hl, $9de0
    call memcpy
    ld de, $15
    ld bc, island_map_data + ($14 * $10)
    ld hl, $9e00
    call memcpy
    ld de, $15
    ld bc, island_map_data + ($14 * $11)
    ld hl, $9e20
    call memcpy
    ret