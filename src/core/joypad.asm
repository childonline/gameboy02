section "joypad", rom0

read_joypad::

    ; get action buttons
    ld a, JOYPAD_BUTTONS    ; choose bit that'll give us action button info
    ld [rJOYPAD], a         ; write to joypad, telling it we'd like button info
    ld a, [rJOYPAD]         ; gameboy will write (back in address) joypad info
    cpl                     ; take complement $FF - a
    and $0F                 ; look at first 4 bits only  (lower nibble)
    swap a                  ; place lower nibble into upper nibble
    ld b, a                 ; store keys in b
    
    ; get directional keys
    ld a, JOYPAD_ARROWS
    ld [rJOYPAD], a         ; write to joypad, selecting direction keys
    ld a, [rJOYPAD]
    cpl                     ; take complement $FF - a
    and $0F                 ; keep lower nibble
    or b                    ; combine action & direction keys (result in a)
    ld b, a

    ld a, JOYPAD_BUTTONS | JOYPAD_ARROWS
    ld [rJOYPAD], a ; reset joypad

    ld a, b ; register A holds result. Each bit represents a key
    ret
