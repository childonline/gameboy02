memcpy::
    dec de
.memcpy_loop:
    ld a, [bc]
    ld [hli], a
    inc bc
    dec de
.memcpy_check_limit:
    ld a, e
    cp a, $00
    jr nz, .memcpy_loop
    ld a, d
    cp a, $00
    jr nz, .memcpy_loop
    ret
