lcd_off::
    ld hl, rLCDC
    res 7, [hl]
    ret

lcd_on::
    ld hl, rLCDC
    set 7, [hl]
    ret

lcd_ini::
    ; disable window & bg display and enable only bg display
    ld hl, rLCDC
    set 1, [hl] ; objects - on
    res 2, [hl] ; 8x8 sprites - on
    set 3, [hl] ; bg display - on
    res 4, [hl] ; bg & window display - off

    ; set bg palette
    ld hl, rBGP
    ld [hl], %11100100

    ; set obj palette 1
    ld hl, rOBP0
    ld [hl], %00011011

    ; set obj palette 2
    ld hl, rOBP1
    ld [hl], %11100100

    ret

wait_vblank::
    ld a, [rLY]
    cp 145
    jr nz, wait_vblank
    ret

clear_oam::
    ld hl, _OAMRAM
    ld c,$A0
.clear_oam_loop:
    ld [hl],0
    inc hl
    dec c
    jp nz,.clear_oam_loop
    ret

clear_bg_map::
    ld hl, _SCRN0
    ld bc, 1024
.clear_bg_map_loop:
    ld a, $00
    ld [hl+], a
    dec bc
    ld a, b
    or a, c
    jr nz, .clear_bg_map_loop
    ret